﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblCurrentTime = New System.Windows.Forms.Label()
        Me.lblTimeTakenLabel = New System.Windows.Forms.Label()
        Me.lblCurrTimeLabel = New System.Windows.Forms.Label()
        Me.lblStatusLabel = New System.Windows.Forms.Label()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.lblScoring = New System.Windows.Forms.Label()
        Me.tmrElapsedTime = New System.Windows.Forms.Timer(Me.components)
        Me.lblTimeElapsed = New System.Windows.Forms.Label()
        Me.tmrCurrentTime = New System.Windows.Forms.Timer(Me.components)
        Me.lblStartTime = New System.Windows.Forms.Label()
        Me.lblStartTimeLabel = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblCurrentTime
        '
        Me.lblCurrentTime.AutoSize = True
        Me.lblCurrentTime.Location = New System.Drawing.Point(239, 170)
        Me.lblCurrentTime.Name = "lblCurrentTime"
        Me.lblCurrentTime.Size = New System.Drawing.Size(41, 39)
        Me.lblCurrentTime.TabIndex = 15
        Me.lblCurrentTime.Text = "..."
        '
        'lblTimeTakenLabel
        '
        Me.lblTimeTakenLabel.AutoSize = True
        Me.lblTimeTakenLabel.Location = New System.Drawing.Point(35, 289)
        Me.lblTimeTakenLabel.Name = "lblTimeTakenLabel"
        Me.lblTimeTakenLabel.Size = New System.Drawing.Size(174, 39)
        Me.lblTimeTakenLabel.TabIndex = 14
        Me.lblTimeTakenLabel.Text = "Time Taken:"
        '
        'lblCurrTimeLabel
        '
        Me.lblCurrTimeLabel.AutoSize = True
        Me.lblCurrTimeLabel.Location = New System.Drawing.Point(35, 170)
        Me.lblCurrTimeLabel.Name = "lblCurrTimeLabel"
        Me.lblCurrTimeLabel.Size = New System.Drawing.Size(198, 39)
        Me.lblCurrTimeLabel.TabIndex = 13
        Me.lblCurrTimeLabel.Text = "Current Time:"
        '
        'lblStatusLabel
        '
        Me.lblStatusLabel.AutoSize = True
        Me.lblStatusLabel.Location = New System.Drawing.Point(35, 98)
        Me.lblStatusLabel.Name = "lblStatusLabel"
        Me.lblStatusLabel.Size = New System.Drawing.Size(108, 39)
        Me.lblStatusLabel.TabIndex = 12
        Me.lblStatusLabel.Text = "Status:"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(113, 98)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(140, 39)
        Me.lblStatus.TabIndex = 11
        Me.lblStatus.Text = "Waiting..."
        '
        'lblScoring
        '
        Me.lblScoring.AutoSize = True
        Me.lblScoring.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScoring.Location = New System.Drawing.Point(35, 28)
        Me.lblScoring.Name = "lblScoring"
        Me.lblScoring.Size = New System.Drawing.Size(240, 39)
        Me.lblScoring.TabIndex = 10
        Me.lblScoring.Text = "Scoring Accounts"
        '
        'tmrElapsedTime
        '
        Me.tmrElapsedTime.Interval = 1000
        '
        'lblTimeElapsed
        '
        Me.lblTimeElapsed.AutoSize = True
        Me.lblTimeElapsed.Location = New System.Drawing.Point(239, 289)
        Me.lblTimeElapsed.Name = "lblTimeElapsed"
        Me.lblTimeElapsed.Size = New System.Drawing.Size(284, 39)
        Me.lblTimeElapsed.TabIndex = 16
        Me.lblTimeElapsed.Text = "0 minutes 0 seconds"
        '
        'tmrCurrentTime
        '
        Me.tmrCurrentTime.Interval = 1000
        '
        'lblStartTime
        '
        Me.lblStartTime.AutoSize = True
        Me.lblStartTime.Location = New System.Drawing.Point(239, 229)
        Me.lblStartTime.Name = "lblStartTime"
        Me.lblStartTime.Size = New System.Drawing.Size(41, 39)
        Me.lblStartTime.TabIndex = 18
        Me.lblStartTime.Text = "..."
        '
        'lblStartTimeLabel
        '
        Me.lblStartTimeLabel.AutoSize = True
        Me.lblStartTimeLabel.Location = New System.Drawing.Point(35, 229)
        Me.lblStartTimeLabel.Name = "lblStartTimeLabel"
        Me.lblStartTimeLabel.Size = New System.Drawing.Size(161, 39)
        Me.lblStartTimeLabel.TabIndex = 17
        Me.lblStartTimeLabel.Text = "Start Time:"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(16.0!, 39.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(715, 357)
        Me.Controls.Add(Me.lblCurrentTime)
        Me.Controls.Add(Me.lblTimeTakenLabel)
        Me.Controls.Add(Me.lblCurrTimeLabel)
        Me.Controls.Add(Me.lblStatusLabel)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.lblScoring)
        Me.Controls.Add(Me.lblTimeElapsed)
        Me.Controls.Add(Me.lblStartTime)
        Me.Controls.Add(Me.lblStartTimeLabel)
        Me.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "Form1"
        Me.Text = "Collections Scoring"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblCurrentTime As Label
    Friend WithEvents lblTimeTakenLabel As Label
    Friend WithEvents lblCurrTimeLabel As Label
    Friend WithEvents lblStatusLabel As Label
    Friend WithEvents lblStatus As Label
    Friend WithEvents lblScoring As Label
    Friend WithEvents tmrElapsedTime As Timer
    Friend WithEvents lblTimeElapsed As Label
    Friend WithEvents tmrCurrentTime As Timer
    Friend WithEvents lblStartTime As Label
    Friend WithEvents lblStartTimeLabel As Label
End Class
