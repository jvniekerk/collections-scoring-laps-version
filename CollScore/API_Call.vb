﻿Module API_Call
    Public Function sendJson(ByVal url As String, ByVal request As String) As String
        ' Set and Instantiate the working objects
        Dim ObjHTTP As Object = CreateObject("MSXML2.ServerXMLHTTP")

        ' Invoke the web service
        With ObjHTTP
            .open("POST", url, False)
            .setRequestHeader("Content-Type", "application/json; charset=utf-8")
            .setTimeouts(60000, 60000, 1200000, 1200000)
            .send(request)

            Dim myString As String = .responseText
            Dim status As Integer = .status.ToString()

            If status = 200 Then
                Return myString
            Else
                Return "Status code: " & status
            End If
        End With
        ObjHTTP = Nothing
    End Function

End Module
