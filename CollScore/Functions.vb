﻿Option Explicit On
Imports System.Data.SqlClient

Module Functions
    Public Sub saveTextFile(ByVal fileName As String, ByVal data As String)
        If System.IO.File.Exists(fileName) Then
            System.IO.File.Delete(fileName)
        End If

        My.Computer.FileSystem.WriteAllText(fileName, data, True)
    End Sub

    Public Function XMLString(ByVal myReader As SqlDataReader) As String
        Dim results As String = ""
        Dim fields As Integer = myReader.FieldCount - 1
        Dim thisString As String = ""

        Do While myReader.Read()
            With myReader
                results = results & "<insertOnly"
                For k As Integer = 0 To fields
                    thisString = .GetString(k)
                    results = results & " " & .GetName(k) & "=""" &
                        Replace(Replace(Replace(Replace(thisString, ">", "&gt;"), "<", "&lt;"), "&", "&amp;"), "%", "&#37;") & """"
                Next
                results = results & "/>"
            End With
        Loop

        myReader.Close()
        Return results
    End Function
End Module
