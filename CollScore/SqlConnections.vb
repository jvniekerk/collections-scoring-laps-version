﻿Option Explicit On
Imports System.Data.SqlClient

Module SqlConnections
    Function sqlToDataTable(ByVal sqlQuery As String) As DataTable
        Dim myConn As SqlConnection
        Dim myCmd As SqlCommand
        Dim myReader As SqlDataReader

        myConn = New SqlConnection(GetConnectionString())
        myCmd = myConn.CreateCommand
        myCmd.CommandTimeout = 0
        myCmd.CommandText = sqlQuery
        myConn.Open()
        myReader = myCmd.ExecuteReader()

        Dim results As New DataTable
        results.Load(myReader)

        myReader.Close()
        myReader = Nothing
        myConn.Close()
        myConn = Nothing

        Return results
    End Function

    Function GetConnectionString() As String
        Dim hostname = System.Net.Dns.GetHostName()
        Dim ip As String = ""
        'Dim connString As String = "Initial Catalog=DiallerStagingArea;Data Source="

        For Each hostAdr In System.Net.Dns.GetHostEntry(hostname).AddressList()
            ip = hostAdr.ToString

            If ip = "172.27.63.23" Or ip = "172.27.63.17" Then
                Return "Initial Catalog=DiallerStagingArea;Data Source=UB-SQLCLSTR-1;User ID=SA;Password=u3zD2Ix6;Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"
                '    connString += "UB-SQLCLSTR-1"
                'Else
                '    connString += "172.27.63.23"
            End If
        Next hostAdr

        'connString += ";User ID=SA;Password=u3zD2Ix6;Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"

        Return "Initial Catalog=DiallerStagingArea;Data Source=172.27.63.23;User ID=SA;Password=u3zD2Ix6;Connect Timeout=0;Connection Timeout=0;Integrated Security=SSPI;"
        'Return connString
    End Function

    Function ConnectToSql(ByVal sqlQuery As String, Optional ByVal download As Boolean = True) As String
        Dim myConn As SqlConnection
        Dim myCmd As SqlCommand
        Dim myReader As SqlDataReader
        Dim results As String = ""

        'MsgBox(sqlQuery)

        myConn = New SqlConnection(GetConnectionString())
        myCmd = myConn.CreateCommand
        myCmd.CommandTimeout = 0
        myCmd.CommandText = sqlQuery
        myConn.Open()
        myReader = myCmd.ExecuteReader()

        If download Then
            results = XMLString(myReader)
        Else
            results = ReturnValue(myReader)
        End If

        myReader.Close()
        myReader = Nothing
        myConn.Close()
        myConn = Nothing

        Return results
    End Function

    Public Function ReturnValue(ByVal myReader As SqlDataReader) As String
        Dim results As String = ""
        Dim fields As Integer = myReader.FieldCount - 1
        Dim thisString As String = ""

        Do While myReader.Read()
            With myReader
                For k As Integer = 0 To fields
                    If k > 1 Then
                        results = " " & results & .GetString(k)
                    Else
                        results = results & .GetString(k)
                    End If
                Next
            End With
        Loop

        myReader.Close()
        Return results
    End Function
End Module
