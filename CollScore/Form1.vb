﻿Option Explicit On

Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim LGurl As String = "https://lg.unclebuckloans.co.uk/api/inference/prod"
        Dim LAPSurl As String = "https://api.unclebuck.tv/api/AuxiliaryServices/AddPersonNote?api_key=ckU1eJldjal0SJU3Xed7Ct"
        Dim query As String
        Dim thisTable As DataTable
        Dim returnTable As DataTable
        Dim request As String
        Dim response As String
        Dim rows As Integer = 1
        Dim counter As Integer = 1
        Dim status As String

        Show()
        Application.DoEvents()

        lblCurrentTime.Text = Now.ToString("dd MMM yyyy HH:mm:ss")
        lblStartTime.Text = Now.ToString("dd MMM yyyy HH:mm:ss")
        Call ResetTimer()

        'With tmrElapsedTime
        '    .Enabled = True
        '    .Start()
        'End With

        'With tmrCurrentTime
        '    .Enabled = True
        '    .Start()
        'End With

        status = "Started..."
        lblStatus.Text = status
        Call RefreshLables()

        'Threading.Thread.Sleep(2000)
        Call RefreshLables()
        'MsgBox("About to start")

        While rows > 0
            'Threading.Thread.Sleep(1000)

            status = "Getting accounts for run number " & counter.ToString & "..."
            lblStatus.Text = status
            Call RefreshLables()

            query = "exec JosefTestingDB.dbo.sp_CollectionsScoring_GetAccounts"
            thisTable = sqlToDataTable(query)
            rows = thisTable.Rows.Count

            'MsgBox(rows.ToString & " rows")
            status = "Processing run number " & counter.ToString & " with " & rows.ToString & " rows..."
            lblStatus.Text = status
            Call RefreshLables()

            If rows > 0 Then
                'MsgBox("Starting scoring")

                For i As Integer = 0 To rows - 1
                    status = "Processing run number " & counter.ToString & " with " & rows.ToString &
                        " rows... -> row number: " & (i + 1).ToString & "..."
                    lblStatus.Text = status
                    Call RefreshLables()

                    request = thisTable.Rows(i).Item(1).ToString
                    response = sendJson(LGurl, request)

                    status = "Processing run number " & counter.ToString & " with " & rows.ToString &
                        " rows... -> row number: " & (i + 1).ToString & "... score received..."
                    lblStatus.Text = status
                    Call RefreshLables()

                    query = "exec JosefTestingDB.dbo.sp_CollectionsScoring_AddRow 'LR-406', '" & response & "'"
                    returnTable = sqlToDataTable(query)

                    status = "Processing run number " & counter.ToString & " with " & rows.ToString &
                        " rows... -> row number: " & (i + 1).ToString & "... row added to log..."
                    lblStatus.Text = status
                    Call RefreshLables()

                    request = "{ ""LoanId"": """ & returnTable.Rows(0).Item(0).ToString & """, ""Note"": """ & returnTable.Rows(0).Item(1).ToString
                    request += """, ""UserId"": 3, ""NoteLevelId"": 0, ""NoteHeaderId"": 308 }"
                    'MsgBox(request)
                    Call sendJson(LAPSurl, request)

                    status = "Processing run number " & counter.ToString & " with " & rows.ToString &
                        " rows... -> row number: " & (i + 1).ToString & "... note recorded..."
                    lblStatus.Text = status
                    Call RefreshLables()

                    returnTable.Clear()

                    'Dim filename As String = "C:\Users\jvniekerk.UNCLEBUCK\Desktop\Project-files\collScore\testResponse" & i.ToString & ".txt"
                    'Call saveTextFile(filename, response)
                Next i

                status = "Run number " & counter.ToString & " completed with " & rows.ToString & " rows..."
                lblStatus.Text = status
                Call RefreshLables()
            End If

            thisTable.Clear()

            'MsgBox("Run number " & counter.ToString & " completed with " & rows.ToString & " rows")
            counter += 1
        End While


        query = "exec JosefTestingDB.dbo.sp_CollectionsScoringAllocateStrategy "
        thisTable = sqlToDataTable(query)
        rows = thisTable.Rows.Count

        If rows > 0 Then
            For i As Integer = 0 To rows - 1
                status = thisTable.Rows(i).Item(2).ToString & " -> strategy added to account"
                request = "{ ""LoanId"": """ & thisTable.Rows(i).Item(0).ToString & """, ""Note"": """ & status.ToString &
                    """, ""UserId"": 3, ""NoteLevelId"": 0, ""NoteHeaderId"": " & thisTable.Rows(i).Item(3).ToString & " }"
                'MsgBox(request)
                Call sendJson(LAPSurl, request)
            Next i
        End If




        status = "Complete..."
        lblStatus.Text = status
        Call RefreshLables()

        'MsgBox("Complete...")

        Close()
    End Sub

    Sub RefreshLables()
        Call ProgressTimerUpdate()
        Call CurrDateTimerUpdate()

        lblTimeElapsed.Refresh()
        lblStatus.Refresh()
        lblCurrentTime.Refresh()
        lblStartTime.Refresh()
    End Sub

    Sub ResetTimer()
        With lblTimeElapsed
            .Text = "0 minutes 0 seconds"
            .Refresh()
        End With
    End Sub

    Sub ProgressTimerUpdate()
        'Dim thisText As String = lblTimeElapsed.Text.ToString
        'thisText = thisText.Replace(" seconds", "")
        'Dim mins As Integer = Microsoft.VisualBasic.Left(thisText, thisText.IndexOf(" minutes"))
        'Dim secs As Integer = thisText.Replace(mins.ToString & " minutes ", "") + 1

        'If secs > 59 Then
        '    mins += 1
        '    secs -= 60
        'End If


        Dim mins As Integer
        Dim secs As Integer
        Dim startTime As Date

        startTime = lblStartTime.Text
        secs = DateDiff(DateInterval.Second, startTime, Now)
        mins = (secs - secs Mod 60) / 60
        secs = secs Mod 60

        With lblTimeElapsed
            .Text = mins.ToString & " minutes " & secs.ToString & " seconds"
            .Refresh()
        End With
    End Sub

    Sub CurrDateTimerUpdate()
        With lblCurrentTime
            .Text = Now.ToString("dd MMM yyyy HH:mm:ss")
            .Refresh()
        End With
    End Sub

    Private Sub tmrElapsedTime_Tick(sender As Object, e As EventArgs) Handles tmrElapsedTime.Tick
        Call ProgressTimerUpdate()
        Call RefreshLables()
    End Sub

    Private Sub tmrCurrentTime_Tick(sender As Object, e As EventArgs) Handles tmrCurrentTime.Tick
        Call CurrDateTimerUpdate()
        Call RefreshLables()
    End Sub
End Class
